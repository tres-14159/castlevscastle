# -*- coding: utf-8 -*- 

class Player:
	__castle = 30
	__wall = 10
	
	__builders = 2
	__bricks = 5
	
	__soldiers = 2
	__weapons = 5
	
	__wizards = 2
	__crystals = 5
	
	def __init__(self, castle = 10, wall = 10, builders = 2, bricks = 5, soldiers = 2, weapons = 5, wizards = 2, crystals = 5):
		self.__hand = []
		self.__castle = castle
		self.__wall = wall
		
		self.__builders = builders
		self.__bricks = bricks
		self.__soldiers = soldiers
		self.__weapons = weapons
		self.__wizards = wizards
		self.__crystals = crystals
	
	def attack(self, amount):
		rest = self.decreaseWall(amount)
		self.decreaseCastle(rest)
	
	def getCastle(self):
		return self.__castle
	
	def getWall(self):
		return self.__wall
	
	def getBuilders(self):
		return self.__builders
	
	def getBricks(self):
		return self.__bricks
	
	def getSoldiers(self):
		return self.__soldiers
	
	def getWeapons(self):
		return self.__weapons
	
	def getWizards(self):
		return self.__wizards
	
	def getCrystals(self):
		return self.__crystals
	
	def decreaseCastle(self, amount):
		self.__castle -= amount
		if self.__castle < 0:
			self.__castle = 0
	
	def increaseCastle(self, amount):
		self.__castle += amount
	
	def decreaseWall(self, amount):
		self.__wall -= amount
		if self.__wall < 0:
			returnValue = self.__wall
			self.__wall = 0
			
			return returnValue
		else:
			return 0
	
	def increaseWall(self, amount):
		self.__wall += amount
	
	def decreaseBuilders(self, amount):
		self.__builders -= amount
		if self.__builders < 0:
			self.__builders = 0
	
	def increaseBuilders(self, amount):
		self.__builders += amount
	
	def decreaseBricks(self, amount):
		self.__bricks -= amount
		if self.__bricks < 0:
			returnValue = self.__bricks
			self.__bricks = 0
			
			return returnValue
		else:
			return 0
	
	def increaseBricks(self, amount):
		self.__bricks += amount
	
	def decreaseSoldiers(self, amount):
		self.__soldiers -= amount
		if self.__soldiers < 0:
			self.__soldiers = 0
	
	def increaseSoldiers(self, amount):
		self.__soldiers += amount
	
	def decreaseWeapons(self, amount):
		self.__weapons -= amount
		if self.__weapons < 0:
			returnValue = self.__weapons
			self.__weapons = 0
			
			return returnValue
		else:
			return 0
	
	def increaseWeapons(self, amount):
		self.__weapons += amount
	
	def decreaseWizards(self, amount):
		self.__wizards -= amount
		if self.__wizards < 0:
			self.__wizards = 0
	
	def increaseWizards(self, amount):
		self.__wizards += amount
	
	def decreaseCrystals(self, amount):
		self.__crystals -= amount
		if self.__crystals < 0:
			returnValue = self.__crystals
			self.__crystals = 0
			
			return returnValue
		else:
			return 0
	
	def increaseCrystals(self, amount):
		self.__crystals += amount
	
	def addCardHand(self, card):
		self.__hand.append(card)
	
	def getHand(self):
		return self.__hand
	
	def getCardHand(self, pos):
		return self.__hand[pos]
	
	def removeCardHand(self, pos):
		return self.__hand.pop(pos)
