The purpose of this game is to be the first to build a 100 storey
castle, you can also win by destroying your opponent castle.

Cards:
To use a card, just click on it. To discard one, hold down CTRL and
click on the card you want to discard.

type of                            how many
matterial                          material
needed         ------+        +---- needed
                     |        |
                     |        |
                     V        V
                   +------------+
                   | St      18 |
                   |            |
                   |            |
                   |            |
                   |            |
                   |            |     What the
                   | Castle +20 | <-- card does
                   +------------+

The card above cost 18 bricks to use, and it will add an additional 20
to your castle's height.

Discard loose the turn and give a new card for the discard card.