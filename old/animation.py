# -*- coding: utf-8 -*- 

import cards, pygame, random, sys, ConfigParser
from pygame.locals import *

import game
from utils import *

class MySprite(pygame.sprite.Sprite):
	def __init__(self, image = None, rect = pygame.Rect(0, 0, 0, 0), imageSurface = None):
		pygame.sprite.Sprite.__init__(self) #call Sprite initializer
		if image != None:
			self.image = pygame.image.load(image)
		else:
			self.image = imageSurface
		self.rect = rect
		
	def update(self, x, y):
		self.rect = self.rect.move(x, y)

class Animation:
	__metaclass__ = Singleton
	
	__framesPerSecond = None
	__clock = None
	
	__screen = None
	__screenWidth = None
	__screenHeight = None
	
	__backgroundGroupSprite = None
	__listCloudGroups = None
	__grassGroupSprite = None
	__castleRedGroupSprite = None
	__castleBlueGroupSprite = None
	__wallRedGroupSprite = None
	__wallBlueGroupSprite = None
	__lightMapGroupSprite = None
	__titleGroupSprite = None
	__listOptionsMainMenu = None
	__wipMessageGroupSprite = None
	
	__timesMovingCloud = 0
	__selectCloud = None
	
	def __init__(self, framesPerSecond = 60):
		self.__framesPerSecond = framesPerSecond
		self.__clock = pygame.time.Clock()
		self.initAnimation()
		
	def waitFrame(self):
		self.__clock.tick(self.__framesPerSecond)
		
	def loadSprites(self):
		gameMachine = game.Game()
		config = ConfigParser.RawConfigParser()
		config.read(gameMachine.getConf('system_graphics') + 'positions.conf')
		
		#Load Positions
		marginScreenCastle = config.getint('General', 'margin_screen_castle')
		marginCastleWall = config.getint('General', 'margin_castle_wall')
		marginTopTitle = config.getint('General', 'margin_top_title')
		marginTop = config.getint('General', 'margin_top')
		marginBetweenTitleAndMenu = config.getint('General', 'margin_between_title_and_menu')
		
		backgroundImage1 = MySprite(gameMachine.getConf('system_graphics') + 'background.png')
		backgroundImage2 = MySprite(gameMachine.getConf('system_graphics') + 'background.png',
			pygame.Rect(0, backgroundImage1.image.get_height(), 0, 0))
		
		self.__backgroundGroupSprite = pygame.sprite.Group()
		self.__backgroundGroupSprite.add(backgroundImage1, backgroundImage2)
		
		lightMapImage1 = MySprite(gameMachine.getConf('system_graphics') + 'lightMap.png')
		lightMapImage2 = MySprite(gameMachine.getConf('system_graphics') + 'lightMap.png',
			pygame.Rect(0, lightMapImage1.image.get_height(), 0, 0))
		self.__lightMapGroupSprite = pygame.sprite.Group()
		self.__lightMapGroupSprite.add(lightMapImage1, lightMapImage2)
		
		self.__listCloudGroups = [pygame.sprite.GroupSingle(MySprite(gameMachine.getConf('system_graphics') + 'cloud00.png',
				pygame.Rect(random.randint(0, self.__screenWidth), random.randint(0, self.__screenHeight/2), 0, 0))),
			pygame.sprite.GroupSingle(MySprite(gameMachine.getConf('system_graphics') + 'cloud01.png',
				pygame.Rect(random.randint(0, self.__screenWidth), random.randint(0, self.__screenHeight/2), 0, 0))),
			pygame.sprite.GroupSingle(MySprite(gameMachine.getConf('system_graphics') + 'cloud02.png',
				pygame.Rect(random.randint(0, self.__screenWidth), random.randint(0, self.__screenHeight/2), 0, 0))),
			pygame.sprite.GroupSingle(MySprite(gameMachine.getConf('system_graphics') + 'cloud03.png',
				pygame.Rect(random.randint(0, self.__screenWidth), random.randint(0, self.__screenHeight/2), 0, 0))),
			pygame.sprite.GroupSingle(MySprite(gameMachine.getConf('system_graphics') + 'cloud04.png', 
				pygame.Rect(random.randint(0, self.__screenWidth), random.randint(0, self.__screenHeight/2), 0, 0)))]
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'grass.png')
		tempSprite.rect = pygame.Rect(0, self.__screenHeight - tempSprite.image.get_height(),0,0)
		grassPositionTop = tempSprite.rect.top
		self.__grassGroupSprite = pygame.sprite.GroupSingle(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'castle_red.png')
		tempSprite.rect = pygame.Rect(marginScreenCastle, grassPositionTop - tempSprite.image.get_height(),0,0)
		self.__castleRedGroupSprite = pygame.sprite.GroupSingle(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'castle_blue.png')
		tempSprite.rect = pygame.Rect(self.__screenWidth - tempSprite.image.get_width() - marginScreenCastle,
			grassPositionTop - tempSprite.image.get_height(),0,0)
		self.__castleBlueGroupSprite = pygame.sprite.GroupSingle(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'wall_red.png')
		tempSprite.rect =pygame.Rect(self.__castleRedGroupSprite.sprites()[0].rect.left + self.__castleRedGroupSprite.sprites()[0].image.get_width() + marginCastleWall,
			grassPositionTop - tempSprite.image.get_height(),0,0)
		self.__wallRedGroupSprite = pygame.sprite.GroupSingle(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'wall_blue.png')
		tempSprite.rect = pygame.Rect(self.__castleBlueGroupSprite.sprites()[0].rect.left - tempSprite.image.get_width() - marginCastleWall,
			grassPositionTop - tempSprite.image.get_height(),0,0)
		self.__wallBlueGroupSprite = pygame.sprite.GroupSingle(tempSprite)
		
		tempSpriteTitle = MySprite(gameMachine.getConf('system_graphics') + 'castle_vs_castle.png')
		tempSpriteTitle.rect = pygame.Rect((self.__screenWidth - tempSpriteTitle.image.get_width()) / 2, marginTopTitle, 0, 0)
		self.__titleGroupSprite = pygame.sprite.GroupSingle(tempSpriteTitle)
		
		arialFont = pygame.font.SysFont('Arial', 40)
		
		posY = tempSpriteTitle.rect.top + tempSpriteTitle.image.get_height() + marginBetweenTitleAndMenu
		self.__listOptionsMainMenu = {}
		self.__listOptionsMainMenu["1_player"] = {}
		tempImage =  arialFont.render("1 Player", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["1_player"]["off"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("1 Player", True, (255, 0, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["1_player"]["over"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("1 Player", True, (255, 255, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["1_player"]["on"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		
		posY = posY + tempImage.get_height() + marginTop
		self.__listOptionsMainMenu["2_player"] = {}
		tempImage =  arialFont.render("2 Player", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["2_player"]["off"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("2 Player", True, (255, 0, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["2_player"]["over"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("2 Player", True, (255, 255, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["2_player"]["on"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		
		posY = posY + tempImage.get_height() + marginTop
		self.__listOptionsMainMenu["net_play"] = {}
		tempImage =  arialFont.render("Net play", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["net_play"]["off"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Net play", True, (255, 0, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["net_play"]["over"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Net play", True, (255, 255, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["net_play"]["on"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		
		posY = posY + tempImage.get_height() + marginTop
		self.__listOptionsMainMenu["help"] = {}
		tempImage =  arialFont.render("Help", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["help"]["off"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Help", True, (255, 0, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["help"]["over"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Help", True, (255, 255, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["help"]["on"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		
		posY = posY + tempImage.get_height() + marginTop
		self.__listOptionsMainMenu["options"] = {}
		tempImage =  arialFont.render("Options", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["options"]["off"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Options", True, (255, 0, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["options"]["over"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Options", True, (255, 255, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["options"]["on"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		
		posY = posY + tempImage.get_height() + marginTop
		self.__listOptionsMainMenu["exit"] = {}
		tempImage =  arialFont.render("Exit", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["exit"]["off"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Exit", True, (255, 0, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["exit"]["over"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		tempImage =  arialFont.render("Exit", True, (255, 255, 0))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		self.__listOptionsMainMenu["exit"]["on"] = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, tempImage.get_width(), tempImage.get_height()), tempImage))
		
		tempImage =  arialFont.render("Not yet developed", True, (255, 255, 255))
		posX = (self.__screenWidth - tempImage.get_width()) / 2
		posY = (self.__screenHeight - tempImage.get_height()) / 2
		self.__wipMessageGroupSprite = pygame.sprite.GroupSingle(MySprite(None, pygame.Rect(posX, posY, 0, 0), tempImage))
		
	def mouseInMenu(self):
		mouseX , mouseY = pygame.mouse.get_pos()
		gameMachine = game.Game()
		
		state = gameMachine.getState()
		
		if (state == 'menu'):
			for menuIndex, optionMenuGroupSprite in self.__listOptionsMainMenu.items():
				groupSprite = optionMenuGroupSprite['off']
				if (groupSprite.sprite.rect.collidepoint(mouseX, mouseY)):
					return menuIndex
		else: #wip all, exit text
			if (self.__listOptionsMainMenu['exit']['off'].sprite.rect.collidepoint(mouseX, mouseY)):
				return 'exit'
			else:
				return state
			
		return state
	
	def initAnimation(self):
		gameMachine = game.Game()
		
		pygame.init()
		
		self.__screenWidth = gameMachine.getConf('width')
		self.__screenHeight = gameMachine.getConf('height')
		
		pygame.display.set_caption('Castle vs Castle')
		self.__screen = pygame.display.set_mode((self.__screenWidth, self.__screenHeight))
		
		self.loadSprites()
	
	def showBackground(self):
		self.__backgroundGroupSprite.draw(self.__screen)
		self.__backgroundGroupSprite.update(0,-1)
		
		for sprite in self.__backgroundGroupSprite.sprites():
			if -(sprite.rect.top) >= sprite.image.get_height():
				sprite.rect.top = sprite.image.get_height()
			
	def showClouds(self):
		for groupSprite in self.__listCloudGroups:
			groupSprite.draw(self.__screen)

		if self.__timesMovingCloud <= 0 :
			self.__timesMovingCloud = random.randint(40, 200)
			self.__selectCloud = random.randint(0, len(self.__listCloudGroups) - 1)
		
		self.__timesMovingCloud = self.__timesMovingCloud  - 1
		
		selectCloudSprite = self.__listCloudGroups[self.__selectCloud].sprites()[0]
		if selectCloudSprite.rect.left > self.__screenWidth :
			selectCloudSprite.rect.left = 0 - selectCloudSprite.image.get_width()
			selectCloudSprite.rect.top = random.randint(0, self.__screenHeight/2)
		else:
			self.__listCloudGroups[self.__selectCloud].update(1, 0)
			
	def showGrass(self):
		self.__grassGroupSprite.draw(self.__screen)
	
	def showCastles(self):
		gameMachine = game.Game()
		
		lifeCastleRed = gameMachine.getState('castle_red_life')
		lifeCastleBlue = gameMachine.getState('castle_blue_life')
		
		grassPositionTop = self.__grassGroupSprite.sprites()[0].rect.top
		catleRedHeight = self.__castleRedGroupSprite.sprites()[0].image.get_height()
		catleBlueHeight = self.__castleBlueGroupSprite.sprites()[0].image.get_height()
		
		self.__castleRedGroupSprite.sprites()[0].rect.top = grassPositionTop - (catleRedHeight * lifeCastleRed / 100)
		self.__castleBlueGroupSprite.sprites()[0].rect.top = grassPositionTop - (catleBlueHeight * lifeCastleBlue / 100)
		
		self.__castleRedGroupSprite.draw(self.__screen)
		self.__castleBlueGroupSprite.draw(self.__screen)
		
	def showWalls(self):
		gameMachine = game.Game()
		
		lifeWallRed = gameMachine.getState('wall_red_life')
		lifeWallBlue = gameMachine.getState('wall_blue_life')
		
		grassPositionTop = self.__grassGroupSprite.sprites()[0].rect.top
		wallRedHeight = self.__wallRedGroupSprite.sprites()[0].image.get_height()
		wallBlueHeight = self.__wallBlueGroupSprite.sprites()[0].image.get_height()
		
		self.__wallRedGroupSprite.sprites()[0].rect.top = grassPositionTop - (wallRedHeight * lifeWallRed / 100)
		self.__wallBlueGroupSprite.sprites()[0].rect.top = grassPositionTop - (wallBlueHeight * lifeWallBlue / 100)
		
		self.__wallRedGroupSprite.draw(self.__screen)
		self.__wallBlueGroupSprite.draw(self.__screen)
		
	def showLightMap(self):
		self.__lightMapGroupSprite.draw(self.__screen)
		self.__lightMapGroupSprite.update(0,-1)
		
		for sprite in self.__lightMapGroupSprite.sprites():
			if -(sprite.rect.top) >= sprite.image.get_height():
				sprite.rect.top = sprite.image.get_height()
				
	def showMainMenu(self, over, click):
		self.__titleGroupSprite.draw(self.__screen)
		
		for keyMenu, optionMenuGroupSprite in self.__listOptionsMainMenu.items():
			if keyMenu == over:
				optionMenuGroupSprite['over'].draw(self.__screen)
			elif keyMenu == click:
				optionMenuGroupSprite['on'].draw(self.__screen)
			else:
				optionMenuGroupSprite['off'].draw(self.__screen)
	
	def showGameDisplay(self):
		gameMachine = game.Game()
		player1Name = gameMachine.getConf('1_player')
		player2Name = gameMachine.getConf('2_player')
		buildersRed = gameMachine.getState('builders_red')
		buildersBlue = gameMachine.getState('builders_blue')
		bricksRed = gameMachine.getState('bricks_red')
		bricksBlue = gameMachine.getState('bricks_blue')
		soldiersRed = gameMachine.getState('soldiers_red')
		soldiersBlue = gameMachine.getState('soldiers_blue')
		weaponsRed = gameMachine.getState('weapons_red')
		weaponsBlue = gameMachine.getState('weapons_blue')
		wizardsRed = gameMachine.getState('wizards_red')
		wizardsBlue = gameMachine.getState('wizards_blue')
		crystalsRed = gameMachine.getState('crystals_red')
		crystalsBlue = gameMachine.getState('crystals_blue')
		
		marginLeftDisplay = 20
		marginRightDisplay = 20
		marginTopDisplay = 30
		marginRightDisplay = 20
		marginBetweenBoxes = 10
		
		arialFont = pygame.font.SysFont('Arial', 20, False, True)
		player1NameImage = arialFont.render(player1Name, True, (255, 255, 255))
		player2NameImage = arialFont.render(player2Name, True, (255, 255, 255))
		
		gameDisplay = pygame.sprite.Group()
		
		gameDisplay.add(MySprite(None, pygame.Rect(marginLeftDisplay, marginTopDisplay, 0, 0), player1NameImage))
		
		posY = marginTopDisplay + player1NameImage.get_height() + marginBetweenBoxes
		gameDisplay.add(MySprite(gameMachine.getConf('system_graphics') + 'info_box_brick.png', pygame.Rect(marginLeftDisplay, posY, 0, 0)))
		
		lastSprite = gameDisplay.sprites()[-1]
		
		textImage = arialFont.render(str(buildersRed), True, (255, 255, 0))
		gameDisplay.add(MySprite(None, pygame.Rect(marginLeftDisplay, posY, 0, 0), textImage))
		
		posY = posY + lastSprite.image.get_height() + marginBetweenBoxes
		gameDisplay.add(MySprite(gameMachine.getConf('system_graphics') + 'info_box_sword.png', pygame.Rect(marginLeftDisplay, posY, 0, 0)))
		
		lastSprite = gameDisplay.sprites()[-1]
		posY = posY + lastSprite.image.get_height() + marginBetweenBoxes
		gameDisplay.add(MySprite(gameMachine.getConf('system_graphics') + 'info_box_crystal.png', pygame.Rect(marginLeftDisplay, posY, 0, 0)))
		
		lastSprite = gameDisplay.sprites()[-1]
		posY = posY + lastSprite.image.get_height() + marginBetweenBoxes
		gameDisplay.add(MySprite(gameMachine.getConf('system_graphics') + 'info_box_castle.png', pygame.Rect(marginLeftDisplay, posY, 0, 0)))
		
		posXRightDisplay = self.__screenWidth - player2NameImage.get_width() - marginRightDisplay
		gameDisplay.add(MySprite(None, pygame.Rect(posXRightDisplay, marginTopDisplay, 0, 0), player2NameImage))
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'info_box_brick.png')
		posXRightDisplay = self.__screenWidth - tempSprite.image.get_width() - marginRightDisplay
		posY = marginTopDisplay + player2NameImage.get_height() + marginBetweenBoxes
		heightLast = tempSprite.image.get_height()
		tempSprite.rect = pygame.Rect(posXRightDisplay, posY, 0, 0) 
		gameDisplay.add(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'info_box_sword.png')
		posXRightDisplay = self.__screenWidth - tempSprite.image.get_width() - marginRightDisplay
		posY = posY + heightLast + marginBetweenBoxes
		heightLast = tempSprite.image.get_height()
		tempSprite.rect = pygame.Rect(posXRightDisplay, posY, 0, 0) 
		gameDisplay.add(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'info_box_crystal.png')
		posXRightDisplay = self.__screenWidth - tempSprite.image.get_width() - marginRightDisplay
		posY = posY + heightLast + marginBetweenBoxes
		heightLast = tempSprite.image.get_height()
		tempSprite.rect = pygame.Rect(posXRightDisplay, posY, 0, 0) 
		gameDisplay.add(tempSprite)
		
		tempSprite = MySprite(gameMachine.getConf('system_graphics') + 'info_box_castle.png')
		posXRightDisplay = self.__screenWidth - tempSprite.image.get_width() - marginRightDisplay
		posY = posY + heightLast + marginBetweenBoxes
		heightLast = tempSprite.image.get_height()
		tempSprite.rect = pygame.Rect(posXRightDisplay, posY, 0, 0) 
		gameDisplay.add(tempSprite)
		
		gameDisplay.draw(self.__screen)
		
	def show1PlayerGame(self):
		self.showGameDisplay()
		return
	
	def showWIP(self):
		gameMachine = game.Game()
		
		over = gameMachine.getState('over')
		click = gameMachine.getState('click')
		
		self.__titleGroupSprite.draw(self.__screen)
		self.__wipMessageGroupSprite.draw(self.__screen)
		
		if 'exit' == over:
			self.__listOptionsMainMenu['exit']['over'].draw(self.__screen)
		elif 'exit' == click:
			self.__listOptionsMainMenu['exit']['on'].draw(self.__screen)
		else:
			self.__listOptionsMainMenu['exit']['off'].draw(self.__screen)
		
	def showAnimation(self):
		self.__screen.fill((0, 255, 0)) #for debug
		self.showBackground()
		self.showClouds()
		self.showCastles()
		self.showWalls()
		self.showGrass()
		self.showLightMap()
		
		gameMachine = game.Game()
		
		state = gameMachine.getState()
		over = gameMachine.getState('over')
		click = gameMachine.getState('click')
		if state == 'menu':
			self.showMainMenu(over, click)
		elif state == '1_player':
			self.show1PlayerGame()
		elif state == '2_player':
			self.showWIP()
		elif state == 'net_play':
			self.showWIP()
		elif state == 'options':
			self.showWIP()
		elif state == 'help':
			self.showWIP()
		else:
			self.showMainMenu(over, click)
		
		pygame.display.update()
