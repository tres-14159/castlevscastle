# -*- coding: utf-8 -*-
import sys

import game3

import card3
from deck3 import *

def main():
	gameMachine = game3.Game()
	
	while True:
		if gameMachine.isExit():
			sys.exit(0)
	
if __name__ == '__main__':
    main()
