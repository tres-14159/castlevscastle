# -*- coding: utf-8 -*- 
import pygame, random, sys
from pygame.locals import *

import animation, game

def main():
	gameMachine = game.Game()
	animationMachine = animation.Animation()
	
	while True:
		if gameMachine.isExit():
			sys.exit(0)
		
		animationMachine.showAnimation()
		
		animationMachine.waitFrame()
		
		for e in pygame.event.get():
			if e.type == QUIT:
				gameMachine.refreshState(e.type)
			if e.type == MOUSEMOTION or e.type == MOUSEBUTTONDOWN or e.type == MOUSEBUTTONUP:
				gameMachine.refreshState(e.type)
	
	return
	
if __name__ == '__main__':
    main()
