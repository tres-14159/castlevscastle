# -*- coding: utf-8 -*- 
import random

class Player:
	__castle = 30
	__wall = 10
	
	__builders = 2
	__bricks = 5
	
	__soldiers = 2
	__weapons = 5
	
	__wizards = 2
	__crystals = 5
	
	__hand = []
	
	def __init__(self, lifeCastle = 10, lifeWall = 10, builders = 2, bricks = 5, soldiers = 2, weapons = 5, wizards = 2, ):
		self.__castle = lifeCastle
		self.__wall = lifeWall
		
	def getAttack(self, damage):
		if self.__wall == 0:
			self.__castle = self.__castle - damage
			if self.__castle < 0:
				self.__castle = 0
		else:
			self.__wall = self.__wall - damage
			if (self.__wall < 0) :
				self.__castle = self.__castle - self.__wall
				self.__wall = 0
		
	def getCastleLife(self):
		return self.__castle
		
	def getWallLife(self):
		return self.__wall
		
	def getBuilders(self):
		return self.__builders
		
	def getBricks(self):
		return self.__bricks
		
	def getSoldiers(self):
		return self.__soldiers
		
	def getWeapons(self):
		return self.__weapons
		
	def getWizards(self):
		return self.__wizards
		
	def getCrystals(self):
		return self.__crystals
		
	def removeCastleLife(self, count):
		overCount = self.__castle - count
		if overCount < 0:
			self.__castle = 0
			return overCount
		else:
			self.__castle = overCount
			return 0
		
	def removeWallLife(self, count):
		overCount = self.__wall - count
		if overCount < 0:
			self.__wall = 0
			return overCount
		else:
			self.__wall = overCount
			return 0
		
	def removeBricks(self, count):
		overCount = self.__bricks - count
		if overCount < 0:
			self.__bricks = 0
			return overCount
		else:
			self.__bricks = overCount
			return 0
			
	def removeWeapons(self, count):
		overCount = self.__weapons - count
		if overCount < 0:
			self.__weapons = 0
			return overCount
		else:
			self.__weapons = overCount
			return 0
			
	def removeCrystals(self, count):
		overCount = self.__crystals - count
		if overCount < 0:
			self.__crystals = 0
			return overCount
		else:
			self.__crystals = overCount
			return 0
			
	def decreaseCastleLife(self, life, overZero = False):
		self.__castle = self.__castle - life
		if not overZero:
			if self.__castle < 0:
				self.__castle = 0
		
	def decreaseWallLife(self, life, overZero = False):
		self.__wall = self.__wall - life
		if not overZero:
			if self.__wall < 0:
				self.__wall = 0
		
	def decreaseBuilders(self, count, overZero = False):
		self.__builders = self.__builders - count
		if not overZero:
			if self.__builders < 0:
				self.__builders = 0
		
	def decreaseBricks(self, count, overZero = False):
		self.__bricks = self.__bricks - count
		if not overZero:
			if self.__bricks < 0:
				self.__bricks = 0
		
	def decreaseSoldiers(self, count, overZero = False):
		self.__soldiers = self.__soldiers - count
		if not overZero:
			if self.__soldiers < 0:
				self.__soldiers = 0
		
	def decreaseWeapons(self, count, overZero = False):
		self.__weapons = self.__weapons - count
		if not overZero:
			if self.__weapons < 0:
				self.__weapons = 0
		
	def decreaseWizards(self, count, overZero = False):
		self.__wizards = self.__wizards - count
		if not overZero:
			if self.__wizards < 0:
				self.__wizards = 0
		
	def decreaseCrystals(self, count, overZero = False):
		self.__crystals = self.__crystals - count
		if not overZero:
			if self.__crystals < 0:
				self.__crystals = 0
			
	def increaseCastleLife(self, life):
		self.__castle = self.__castle + life
		
	def increaseWallLife(self, life):
		self.__wall = self.__wall + life
		
	def increaseBuilders(self, count):
		self.__builders = self.__builders + count
		
	def increaseBricks(self, count):
		self.__bricks = self.__bricks + count
		
	def increaseSoldiers(self, count):
		self.__soldiers = self.__soldiers + count
		
	def increaseWeapons(self, count):
		self.__weapons = self.__weapons + count
		
	def increaseWizards(self, count):
		self.__wizards = self.__wizards + count
		
	def increaseCrystals(self, count):
		self.__crystals = self.__crystals + count
		
	def setCastleLife(self, life):
		self.__castle = life
		
	def setWallLife(self, life):
		self.__wall = life
		
	def setBuilders(self, count):
		self.__builders = count
		
	def setBricks(self, count):
		self.__bricks = count
		
	def setSoldiers(self, count):
		self.__soldiers = count
		
	def setWeapons(self, count):
		self.__weapons = count
		
	def setWizards(self, count):
		self.__wizards = count
		
	def setCrystals(self, count):
		self.__crystals = count
	
	def putHand(self, card):
		self.__hand.append(card)
	
	def getCardHand(self, pos):
		if (pos < len(self.__hand)):
			card = self.__hand[pos]
			del(self.__hand[pos])
			return card
		else:
			return None
		
	def showHand(self):
		return self.__hand

class Card:
	__card = None
	__playerOwn = None
	__playerOther = None
	__costCard = {'wall': {'b': 1, 'w': 0, 'c': 0},
		'base': {'b': 1, 'w': 0, 'c': 0},
		'defence': {'b': 3, 'w': 0, 'c': 0},
		'reserve': {'b': 3, 'w': 0, 'c': 0},
		'tower': {'b': 5, 'w': 0, 'c': 0},
		'school': {'b': 8, 'w': 0, 'c': 0},
		'wain': {'b': 10, 'w': 0, 'c': 0},
		'fence': {'b': 12, 'w': 0, 'c': 0},
		'fort': {'b': 18, 'w': 0, 'c': 0},
		'babylon': {'b': 39, 'w': 0, 'c': 0},
		'archer': {'b': 0, 'w': 1, 'c': 0},
		'knight': {'b': 0, 'w': 2, 'c': 0},
		'rider': {'b': 0, 'w': 2, 'c': 0},
		'platoon': {'b': 0, 'w': 4, 'c': 0},
		'recruit': {'b': 0, 'w': 8, 'c': 0},
		'attack': {'b': 0, 'w': 10, 'c': 0},
		'saboteur': {'b': 0, 'w': 12, 'c': 0},
		'thief': {'b': 0, 'w': 15, 'c': 0},
		'swat': {'b': 0, 'w': 18, 'c': 0},
		'banshee': {'b': 0, 'w': 28, 'c': 0},
		'conjure_bricks': {'b': 0, 'w': 0, 'c': 4},
		'conjure_crystals': {'b': 0, 'w': 0, 'c': 4},
		'conjure_weapons': {'b': 0, 'w': 0, 'c': 4},
		'crush_briks': {'b': 0, 'w': 0, 'c': 4},
		'crush_crystals': {'b': 0, 'w': 0, 'c': 4},
		'crush_weapons': {'b': 0, 'w': 0, 'c': 4},
		'sorcerer': {'b': 0, 'w': 0, 'c': 8},
		'dragon': {'b': 0, 'w': 0, 'c': 21},
		'pixies': {'b': 0, 'w': 0, 'c': 22},
		'curse': {'b': 0, 'w': 0, 'c': 45}}
	
	def __init__(self, card, playerOwn, playerOther):
		self.__card = card
		self.__playerOwn = playerOwn
		self.__playerOther = playerOther
		
	def canUse(self):
		cost = self.__costCard[self.__card]
		print(cost)
		
		if self.__card == 'reserve':
			if self.__playerOwn.getWallLife() < 4:
				return False
		
		if cost['b'] <= self.__playerOwn.getBricks():
			if cost['w'] <= self.__playerOwn.getWeapons():
				if cost['c'] <= self.__playerOwn.getCrystals():
					return True
		
		return False
		
	def payCard(self):
		cost = self.__costCard[self.__card]
		bricks = self.__playerOwn.getBricks()
		weapons = self.__playerOwn.getWeapons()
		crystals = self.__playerOwn.getCrystals()
		
		self.__playerOwn.setBricks(bricks - cost['b'])
		self.__playerOwn.setWeapons(weapons - cost['w'])
		self.__playerOwn.setCrystals(crystals - cost['c'])
		
	def useCard(self):
		if self.__card == 'wall':
			self.wall()
		elif self.__card == 'base':
			self.base()
		elif self.__card == 'defence':
			self.defence()
		elif self.__card == 'reserve':
			self.reserve()
		elif self.__card == 'tower':
			self.tower()
		elif self.__card == 'school':
			self.school()
		elif self.__card == 'wain':
			self.wain()
		elif self.__card == 'fence':
			self.fence()
		elif self.__card == 'fort':
			self.fort()
		elif self.__card == 'babylon':
			self.babylon()
		elif self.__card == 'archer':
			self.archer()
		elif self.__card == 'knight':
			self.knight()
		elif self.__card == 'rider':
			self.rider()
		elif self.__card == 'platoon':
			self.platoon()
		elif self.__card == 'recruit':
			self.recruit()
		elif self.__card == 'attack':
			self.attack()
		elif self.__card == 'saboteur':
			self.saboteur()
		elif self.__card == 'thief':
			self.thief()
		elif self.__card == 'swat':
			self.swat()
		elif self.__card == 'banshee':
			self.bashee()
		elif self.__card == 'conjure_bricks':
			self.conjure_bricks()
		elif self.__card == 'conjure_crystals':
			self.conjure_crystals()
		elif self.__card == 'conjure_weapons':
			self.conjure_weapons()
		elif self.__card == 'crush_briks':
			self.crush_briks()
		elif self.__card == 'crush_crystals':
			self.crush_crystals()
		elif self.__card == 'crush_weapons':
			self.crush_weapons()
		elif self.__card == 'sorcerer':
			self.sorcerer()
		elif self.__card == 'dragon':
			self.dragon()
		elif self.__card == 'pixies':
			self.pixies()
		elif self.__card == 'curse':
			self.curse()
			
	def wall(self):
		self.__playerOwn.increaseWallLife(3)
		
	def base(self):
		self.__playerOwn.increaseCastleLife(2)
		
	def defence(self):
		self.__playerOwn.increaseWallLife(6)
		
	def reserve(self):
		self.__playerOwn.increaseWallLife(-4)
		self.__playerOwn.increaseCastleLife(8)
		
	def tower(self):
		self.__playerOwn.increaseCastleLife(5)
		
	def school(self):
		self.__playerOwn.increaseBuilders(1)
	
	def wain(self):
		self.__playerOther.decreaseCastleLife(4)
		self.__playerOwn.increaseCastleLife(8)
		
	def fence(self):
		wallLife = self.__playerOwn.getWallLife()
		
		self.__playerOwn.setWallLife(wallLife + 22)
		
	def fort(self):
		castleLife = self.__playerOwn.getCastleLife()
		
		self.__playerOwn.setCastleLife(castleLife + 20)
		
	def babylon(self):
		castleLife = self.__playerOwn.getCastleLife()
		
		self.__playerOwn.setCastleLife(castleLife + 32)
		
	def archer(self):
		self.__playerOther.getAttack(2)
		
	def knight(self):
		self.__playerOther.getAttack(3)
		
	def rider(self):
		self.__playerOther.getAttack(4)
		
	def platoon(self):
		self.__playerOther.getAttack(6)
		
	def recruit(self):
		soldiers = self.__playerOwn.getSoldiers()
		self.__playerOwn.setSoldiers(soldiers + 1)
		
	def attack(self):
		self.__playerOther.getAttack(12)
		
	def saboteur(self):
		self.__playerOther.decreaseBricks(4)
		self.__playerOther.decreaseWeapons(4)
		self.__playerOther.decreaseCrystals(4)
		
	def thief(self):
		#This values as over count, when it's negative its for example
		# 3 briks and minus 5 = -2
		overRemoveBricks = self.__playerOther.removeBricks(5)
		overRemoveWeapons = self.__playerOther.removeWeapons(5)
		overRemoveCrystals = self.__playerOther.removeCrystals(5)
		
		self.__playerOwn.increaseBricks(5 + overRemoveBricks)
		self.__playerOwn.increaseWeapons(5 + overRemoveWeapons)
		self.__playerOwn.increaseCrystals(5 + overRemoveCrystals)
		
	def swat(self):
		self.__playerOther.decreaseCastleLife(10)
		
	def bashee(self)
		self.__playerOther.getAttack(32)
		
	def conjure_bricks(self):
		self.__playerOwn.increaseBricks(8)
		
	def conjure_crystals(self):
		self.__playerOwn.increaseCrystals(8)
		
	def conjure_weapons(self):
		self.__playerOwn.increaseWeapons(8)
		
	def crush_briks(self):
		self.__playerOther.decreaseBricks(8)
		
	def crush_crystals(self):
		self.__playerOther.decreaseCrystals(8)
		
	def crush_weapons(self):
		self.__playerOther.decreaseWeapons(8)
		
	def sorcerer(self):
		self.__playerOwn.increaseWizards(8)
		
	def dragon(self):
		self.__playerOther.getAttack(25)
		
	def pixies(self):
		self.__playerOwn.increaseCastleLife(22)
		
	def curse(self):
		self.__playerOther.decreaseCastleLife(1)
		self.__playerOther.decreaseWallLife(1)
		self.__playerOther.decreaseBuilders(1)
		self.__playerOther.decreaseBricks(1)
		self.__playerOther.decreaseSoldiers(1)
		self.__playerOther.decreaseWeapons(1)
		self.__playerOther.decreaseWizards(1)
		self.__playerOther.decreaseCrystals(1)
		
class Deck:
	__pileDown = []
	__pileUp = []
	
	def __init__(self, cards = None):
		if cards == None:
			self.__pileDown = ['wall', 'wall', 'wall', 'base', 'base',
				'base', 'defence', 'defence', 'defence', 'reserve',
				'reserve', 'reserve', 'tower', 'tower', 'tower',
				'school', 'school', 'school', 'wain', 'wain', 'fence',
				'fence', 'fort', 'fort', 'babylon', 'babylon', 'archer',
				'archer', 'archer', 'knight', 'knight', 'knight',
				'rider', 'rider', 'rider', 'platoon', 'platoon',
				'platoon', 'recruit', 'recruit', 'recruit', 'attack',
				'attack', 'saboteur', 'saboteur', 'thief', 'thief',
				'swat', 'swat', 'banshee', 'banshee', 'conjure_bricks',
				'conjure_bricks', 'conjure_bricks', 'conjure_crystals',
				'conjure_crystals', 'conjure_crystals',
				'conjure_weapons', 'conjure_weapons', 'conjure_weapons',
				'crush_briks', 'crush_briks', 'crush_briks',
				'crush_crystals', 'crush_crystals', 'crush_crystals',
				'crush_weapons', 'crush_weapons', 'crush_weapons',
				'sorcerer', 'sorcerer', 'sorcerer', 'dragon', 'dragon',
				'pixies', 'pixies', 'curse', 'curse']
		else:
			self.__pileDown = cards
		
	def top(self):
		self.__pileUp.append(self.__pileDown.pop())
		return self.__pileUp[-1]
		
	def shuffle(self):
		random.shuffle(self.__pileDown)
	
	def pickCard(self):
		return self.__pileDown.pop()

def main():
	player1 = Player()
	player2 = Player()
	deck = Deck()
	
	deck.shuffle()
	
	print(deck._Deck__pileDown)
	player1.putHand(deck.pickCard())
	print(player1.showHand())
	card = player1.getCardHand(0)
	print(player1.showHand())
	objCard = Card('wall', player1, player2)
	print(player1.getBricks())
	print(player1.getWeapons())
	print(player1.getCrystals())
	print(player1.getWallLife())
	print(player1.getCastleLife())
	objCard.canUse()
	objCard.payCard()
	objCard.useCard()
	print(player1.getBricks())
	print(player1.getWeapons())
	print(player1.getCrystals())
	print(player1.getWallLife())
	print(player1.getCastleLife())
	
	return
	
if __name__ == '__main__':
    main()
