# -*- coding: utf-8 -*- 

import pygame, random, sys
from pygame.locals import *

#Sprites
backCard = None
cardsBrickSprites = None

#Sizes
cardWidth = None
cardHeight = None

#data
typeCards = ['bricks', 'sword', 'crystal']

cardsBricksData = [
	{'name': 'Wall', 'cost': {'brick': 1, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Base', 'cost': {'brick': 1, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Defece', 'cost': {'brick': 3, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Reserve', 'cost': {'brick': 3, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Tower', 'cost': {'brick': 5, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'School', 'cost': {'brick': 8, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Wain', 'cost': {'brick': 10, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Wall', 'cost': {'brick': 22, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Fort', 'cost': {'brick': 18, 'sword': 0, 'crystal': 0}, 'action': None},
	{'name': 'Babylon', 'cost': {'brick': 39, 'sword': 0, 'crystal': 0}, 'action': None}]

def init(conf):
	global backCard
	global cardsBrickSprites
	global cardWidth
	global cardHeight
	
	backCard = pygame.image.load(conf['card_graphics'] + 'back_card.png')
	cardsBrickSprites = pygame.image.load(conf['card_graphics'] + 'cards_brick.png')
	cardWidth = backCard.get_width()
	cardHeight = backCard.get_height()
	

def getCardSprite(typeCard, numCard):
	areaCard = pygame.Rect(65 * numCard, 0, 65, 100)
	
	if (typeCard == 0):
		return cardsBrickSprites.subsurface(areaCard)
