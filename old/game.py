# -*- coding: utf-8 -*- 

import pygame, random, sys, ConfigParser
from pygame.locals import *

import animation

from utils import *

class Game:
	__metaclass__ = Singleton
	
	__state = None #state of game
	__conf = None #configuration of game
	
	def __init__(self):
		self.__state = {
			'state': "menu", 
			'over': None, 
			'click': None,
			'castle_red_life': 30,
			'castle_blue_life': 30,
			'wall_red_life': 10,
			'wall_blue_life': 10,
			'builders_red': 2,
			'builders_blue': 2,
			'bricks_red': 5,
			'bricks_blue': 5,
			'soldiers_red': 2,
			'soldiers_blue': 2,
			'weapons_red': 5,
			'weapons_blue': 5,
			'wizards_red': 2,
			'wizards_blue': 2,
			'crystals_red': 5,
			'crystals_blue': 5}
		self.loadConf()
			
	def getConf(self, token = None):
		if token == None:
			return self.__conf
		else:
			return self.__conf[token]
		
	def loadConf(self):
		config = ConfigParser.RawConfigParser()
		config.read('data/conf/conf')
		
		self.__conf = {}
		
		try:
			self.__conf['card_graphics'] = 'data/cards/' + config.get('Graphics', 'card_graphics') + '/'
		except ConfigParser.NoOptionError: 
			self.__conf['card_graphics'] = 'data/cards/set_default/'
		try:
			self.__conf['system_graphics'] = 'data/graphics/' + config.get('Graphics', 'card_graphics') + '/'
		except ConfigParser.NoOptionError: 
			self.__conf['system_graphics'] = 'data/graphics/set_default/'
		try:
			self.__conf['full_screen'] = config.getint('Graphics', 'full_screen')
		except ConfigParser.NoOptionError: 
			self.__conf['full_screen'] = 0
		try:
			self.__conf['width'] = config.getint('Graphics', 'width')
		except ConfigParser.NoOptionError: 
			self.__conf['width'] = 800
		try:
			self.__conf['height'] = config.getint('Graphics', 'height')
		except ConfigParser.NoOptionError: 
			self.__conf['height'] = 600
		
		
		try:
			self.__conf['1_player'] = config.get('Other', '1_player').replace('"','')
		except ConfigParser.NoOptionError: 
			self.__conf['1_player'] = '1º Player'
		try:
			self.__conf['2_player'] = config.get('Other', '2_player').replace('"','')
		except ConfigParser.NoOptionError: 
			self.__conf['2_player'] = '2º Player'
	
	def getState(self, token = 'state'):
		return self.__state[token]
		
	def isExit(self):
		if self.__state['state'] == "exit":
			return True
		else:
			return False
			
	def refreshState(self, eventType):
		self.__state['over'] = None
		self.__state['click'] = None
		animationMachine = animation.Animation()
		
		if eventType == QUIT:
			self.__state['state'] = 'exit'
			return
			
		if self.__state['state'] == 'menu':
			optionActivate = animationMachine.mouseInMenu()
			if eventType == MOUSEMOTION:
				self.__state['over'] = optionActivate
				
			if eventType == MOUSEBUTTONDOWN:
				self.__state['click'] = optionActivate
					
			if eventType == MOUSEBUTTONUP:
				self.__state['over'] = optionActivate
				self.__state['state'] = optionActivate
					
				if optionActivate == "exit":
					self.__state['state'] = 'exit'
			return
			
		if self.__state['state'] == '1_player':
			optionActivate = animationMachine.mouseInMenu()
			if eventType == MOUSEMOTION:
				self.__state['over'] = optionActivate
			
			if eventType == MOUSEBUTTONDOWN:
				self.__state['click'] = optionActivate
				
			if eventType == MOUSEBUTTONUP:
				self.__state['over'] = optionActivate
				self.__state['state'] = 'menu'		
			return
			
		if self.__state['state'] == '2_player':
			optionActivate = animationMachine.mouseInMenu()
			if eventType == MOUSEMOTION:
				self.__state['over'] = optionActivate
			
			if eventType == MOUSEBUTTONDOWN:
				self.__state['click'] = optionActivate
				
			if eventType == MOUSEBUTTONUP:
				self.__state['over'] = optionActivate
				self.__state['state'] = 'menu'
			return
			
		if self.__state['state'] == 'net_play':
			optionActivate = animationMachine.mouseInMenu()
			if eventType == MOUSEMOTION:
				self.__state['over'] = optionActivate
			
			if eventType == MOUSEBUTTONDOWN:
				self.__state['click'] = optionActivate
				
			if eventType == MOUSEBUTTONUP:
				self.__state['over'] = optionActivate
				self.__state['state'] = 'menu'			
			return
			
		if self.__state['state'] == 'options':
			optionActivate = animationMachine.mouseInMenu()
			if eventType == MOUSEMOTION:
				self.__state['over'] = optionActivate
			
			if eventType == MOUSEBUTTONDOWN:
				self.__state['click'] = optionActivate
				
			if eventType == MOUSEBUTTONUP:
				self.__state['over'] = optionActivate
				self.__state['state'] = 'menu'						
			return
			
		if self.__state['state'] == 'help':
			optionActivate = animationMachine.mouseInMenu()
			if eventType == MOUSEMOTION:
				self.__state['over'] = optionActivate
			
			if eventType == MOUSEBUTTONDOWN:
				self.__state['click'] = optionActivate
				
			if eventType == MOUSEBUTTONUP:
				self.__state['over'] = optionActivate
				self.__state['state'] = 'menu'						
			return
			
#----------old--------------
state = {
	'state': "menu", 
	'over': None, 
	'click': None,
	'castle_red_life': 30,
	'castle_blue_life': 30,
	'wall_red_life': 10,
	'wall_blue_life': 10}
	
conf = {}

system_animation = None

def getConf():
	return conf

def loadConf():
	global conf
	
	config = ConfigParser.RawConfigParser()
	config.read('data/conf/conf')
	try:
		conf['card_graphics'] = 'data/cards/' + config.get('Graphics', 'card_graphics') + '/'
	except ConfigParser.NoOptionError: 
		conf['card_graphics'] = 'data/cards/set_default/'
	try:
		conf['system_graphics'] = 'data/graphics/' + config.get('Graphics', 'card_graphics') + '/'
	except ConfigParser.NoOptionError: 
		conf['system_graphics'] = 'data/graphics/set_default/'
	
	try:
		conf['1_player'] = config.get('Other', '1_player').replace('"','')
	except ConfigParser.NoOptionError: 
		conf['1_player'] = '1º Player'
	try:
		conf['2_player'] = config.get('Other', '2_player').replace('"','')
	except ConfigParser.NoOptionError: 
		conf['2_player'] = '2º Player'

def initGame(animation):
	global system_animation
	
	system_animation = animation

def getState():
	global state
	
	return state

def refreshState(eventType):
	global state
	
	state['over'] = None
	state['click'] = None
	
	if eventType == QUIT:
		state['state'] = 'exit'
		return
	
	if state['state'] == 'menu':
		optionActivate = system_animation.mouseInMenu()
		if eventType == MOUSEMOTION:
			state['over'] = optionActivate
			
		if eventType == MOUSEBUTTONDOWN:
			state['click'] = optionActivate
				
		if eventType == MOUSEBUTTONUP:
			state['over'] = optionActivate
			state['state'] = optionActivate
				
			if optionActivate == "exit":
				state['state'] = 'exit'
		return
		
	if state['state'] == '1_player':
		optionActivate = system_animation.mouseInMenu()
		if eventType == MOUSEMOTION:
			state['over'] = optionActivate
		
		if eventType == MOUSEBUTTONDOWN:
			state['click'] = optionActivate
			
		if eventType == MOUSEBUTTONUP:
			state['over'] = optionActivate
			state['state'] = 'menu'		
			
		return
		
	if state['state'] == '2_player':
		optionActivate = system_animation.mouseInMenu()
		if eventType == MOUSEMOTION:
			state['over'] = optionActivate
		
		if eventType == MOUSEBUTTONDOWN:
			state['click'] = optionActivate
			
		if eventType == MOUSEBUTTONUP:
			state['over'] = optionActivate
			state['state'] = 'menu'		
			
		return
		
	if state['state'] == 'net_play':
		optionActivate = system_animation.mouseInMenu()
		if eventType == MOUSEMOTION:
			state['over'] = optionActivate
		
		if eventType == MOUSEBUTTONDOWN:
			state['click'] = optionActivate
			
		if eventType == MOUSEBUTTONUP:
			state['over'] = optionActivate
			state['state'] = 'menu'
		
		return

	if state['state'] == 'options':
		optionActivate = system_animation.mouseInMenu()
		if eventType == MOUSEMOTION:
			state['over'] = optionActivate
		
		if eventType == MOUSEBUTTONDOWN:
			state['click'] = optionActivate
			
		if eventType == MOUSEBUTTONUP:
			state['over'] = optionActivate
			state['state'] = 'menu'		
			
		return

	if state['state'] == 'help':
		optionActivate = system_animation.mouseInMenu()
		if eventType == MOUSEMOTION:
			state['over'] = optionActivate
		
		if eventType == MOUSEBUTTONDOWN:
			state['click'] = optionActivate
			
		if eventType == MOUSEBUTTONUP:
			state['over'] = optionActivate
			state['state'] = 'menu'		
			
		return
