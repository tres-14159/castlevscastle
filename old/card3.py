# -*- coding: utf-8 -*-

class Card:
	__card = None
	__playerOwn = None
	__playerOther = None
	__costCard = {'wall': {'b': 1, 'w': 0, 'c': 0},
		'base': {'b': 1, 'w': 0, 'c': 0},
		'defence': {'b': 3, 'w': 0, 'c': 0},
		'reserve': {'b': 3, 'w': 0, 'c': 0},
		'tower': {'b': 5, 'w': 0, 'c': 0},
		'school': {'b': 8, 'w': 0, 'c': 0},
		'wain': {'b': 10, 'w': 0, 'c': 0},
		'fence': {'b': 12, 'w': 0, 'c': 0},
		'fort': {'b': 18, 'w': 0, 'c': 0},
		'babylon': {'b': 39, 'w': 0, 'c': 0},
		'archer': {'b': 0, 'w': 1, 'c': 0},
		'knight': {'b': 0, 'w': 2, 'c': 0},
		'rider': {'b': 0, 'w': 2, 'c': 0},
		'platoon': {'b': 0, 'w': 4, 'c': 0},
		'recruit': {'b': 0, 'w': 8, 'c': 0},
		'attack': {'b': 0, 'w': 10, 'c': 0},
		'saboteur': {'b': 0, 'w': 12, 'c': 0},
		'thief': {'b': 0, 'w': 15, 'c': 0},
		'swat': {'b': 0, 'w': 18, 'c': 0},
		'banshee': {'b': 0, 'w': 28, 'c': 0},
		'conjure_bricks': {'b': 0, 'w': 0, 'c': 4},
		'conjure_crystals': {'b': 0, 'w': 0, 'c': 4},
		'conjure_weapons': {'b': 0, 'w': 0, 'c': 4},
		'crush_briks': {'b': 0, 'w': 0, 'c': 4},
		'crush_crystals': {'b': 0, 'w': 0, 'c': 4},
		'crush_weapons': {'b': 0, 'w': 0, 'c': 4},
		'sorcerer': {'b': 0, 'w': 0, 'c': 8},
		'dragon': {'b': 0, 'w': 0, 'c': 21},
		'pixies': {'b': 0, 'w': 0, 'c': 22},
		'curse': {'b': 0, 'w': 0, 'c': 45}}
	
	def __init__(self, card, playerOwn, playerOther):
		self.__card = card
		self.__playerOwn = playerOwn
		self.__playerOther = playerOther
		
	def canUse(self):
		cost = self.__costCard[self.__card]
		print(cost)
		
		if self.__card == 'reserve':
			if self.__playerOwn.getWallLife() < 4:
				return False
		
		if cost['b'] <= self.__playerOwn.getBricks():
			if cost['w'] <= self.__playerOwn.getWeapons():
				if cost['c'] <= self.__playerOwn.getCrystals():
					return True
		
		return False
		
	def payCard(self):
		cost = self.__costCard[self.__card]
		bricks = self.__playerOwn.getBricks()
		weapons = self.__playerOwn.getWeapons()
		crystals = self.__playerOwn.getCrystals()
		
		self.__playerOwn.setBricks(bricks - cost['b'])
		self.__playerOwn.setWeapons(weapons - cost['w'])
		self.__playerOwn.setCrystals(crystals - cost['c'])
		
	def useCard(self):
		if self.__card == 'wall':
			self.wall()
		elif self.__card == 'base':
			self.base()
		elif self.__card == 'defence':
			self.defence()
		elif self.__card == 'reserve':
			self.reserve()
		elif self.__card == 'tower':
			self.tower()
		elif self.__card == 'school':
			self.school()
		elif self.__card == 'wain':
			self.wain()
		elif self.__card == 'fence':
			self.fence()
		elif self.__card == 'fort':
			self.fort()
		elif self.__card == 'babylon':
			self.babylon()
		elif self.__card == 'archer':
			self.archer()
		elif self.__card == 'knight':
			self.knight()
		elif self.__card == 'rider':
			self.rider()
		elif self.__card == 'platoon':
			self.platoon()
		elif self.__card == 'recruit':
			self.recruit()
		elif self.__card == 'attack':
			self.attack()
		elif self.__card == 'saboteur':
			self.saboteur()
		elif self.__card == 'thief':
			self.thief()
		elif self.__card == 'swat':
			self.swat()
		elif self.__card == 'banshee':
			self.bashee()
		elif self.__card == 'conjure_bricks':
			self.conjure_bricks()
		elif self.__card == 'conjure_crystals':
			self.conjure_crystals()
		elif self.__card == 'conjure_weapons':
			self.conjure_weapons()
		elif self.__card == 'crush_briks':
			self.crush_briks()
		elif self.__card == 'crush_crystals':
			self.crush_crystals()
		elif self.__card == 'crush_weapons':
			self.crush_weapons()
		elif self.__card == 'sorcerer':
			self.sorcerer()
		elif self.__card == 'dragon':
			self.dragon()
		elif self.__card == 'pixies':
			self.pixies()
		elif self.__card == 'curse':
			self.curse()
			
	def wall(self):
		self.__playerOwn.increaseWallLife(3)
		
	def base(self):
		self.__playerOwn.increaseCastleLife(2)
		
	def defence(self):
		self.__playerOwn.increaseWallLife(6)
		
	def reserve(self):
		self.__playerOwn.increaseWallLife(-4)
		self.__playerOwn.increaseCastleLife(8)
		
	def tower(self):
		self.__playerOwn.increaseCastleLife(5)
		
	def school(self):
		self.__playerOwn.increaseBuilders(1)
	
	def wain(self):
		self.__playerOther.decreaseCastleLife(4)
		self.__playerOwn.increaseCastleLife(8)
		
	def fence(self):
		wallLife = self.__playerOwn.getWallLife()
		
		self.__playerOwn.setWallLife(wallLife + 22)
		
	def fort(self):
		castleLife = self.__playerOwn.getCastleLife()
		
		self.__playerOwn.setCastleLife(castleLife + 20)
		
	def babylon(self):
		castleLife = self.__playerOwn.getCastleLife()
		
		self.__playerOwn.setCastleLife(castleLife + 32)
		
	def archer(self):
		self.__playerOther.getAttack(2)
		
	def knight(self):
		self.__playerOther.getAttack(3)
		
	def rider(self):
		self.__playerOther.getAttack(4)
		
	def platoon(self):
		self.__playerOther.getAttack(6)
		
	def recruit(self):
		soldiers = self.__playerOwn.getSoldiers()
		self.__playerOwn.setSoldiers(soldiers + 1)
		
	def attack(self):
		self.__playerOther.getAttack(12)
		
	def saboteur(self):
		self.__playerOther.decreaseBricks(4)
		self.__playerOther.decreaseWeapons(4)
		self.__playerOther.decreaseCrystals(4)
		
	def thief(self):
		#This values as over count, when it's negative its for example
		# 3 briks and minus 5 = -2
		overRemoveBricks = self.__playerOther.removeBricks(5)
		overRemoveWeapons = self.__playerOther.removeWeapons(5)
		overRemoveCrystals = self.__playerOther.removeCrystals(5)
		
		self.__playerOwn.increaseBricks(5 + overRemoveBricks)
		self.__playerOwn.increaseWeapons(5 + overRemoveWeapons)
		self.__playerOwn.increaseCrystals(5 + overRemoveCrystals)
		
	def swat(self):
		self.__playerOther.decreaseCastleLife(10)
		
	def bashee(self):
		self.__playerOther.getAttack(32)
		
	def conjure_bricks(self):
		self.__playerOwn.increaseBricks(8)
		
	def conjure_crystals(self):
		self.__playerOwn.increaseCrystals(8)
		
	def conjure_weapons(self):
		self.__playerOwn.increaseWeapons(8)
		
	def crush_briks(self):
		self.__playerOther.decreaseBricks(8)
		
	def crush_crystals(self):
		self.__playerOther.decreaseCrystals(8)
		
	def crush_weapons(self):
		self.__playerOther.decreaseWeapons(8)
		
	def sorcerer(self):
		self.__playerOwn.increaseWizards(8)
		
	def dragon(self):
		self.__playerOther.getAttack(25)
		
	def pixies(self):
		self.__playerOwn.increaseCastleLife(22)
		
	def curse(self):
		self.__playerOther.decreaseCastleLife(1)
		self.__playerOther.decreaseWallLife(1)
		self.__playerOther.decreaseBuilders(1)
		self.__playerOther.decreaseBricks(1)
		self.__playerOther.decreaseSoldiers(1)
		self.__playerOther.decreaseWeapons(1)
		self.__playerOther.decreaseWizards(1)
		self.__playerOther.decreaseCrystals(1)
