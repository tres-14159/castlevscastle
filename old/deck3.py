# -*- coding: utf-8 -*-
import random

class Deck:
	__pileDown = []
	__pileUp = []
	
	def __init__(self, cards = None):
		if cards == None:
			self.__pileDown = ['wall', 'wall', 'wall', 'base', 'base',
				'base', 'defence', 'defence', 'defence', 'reserve',
				'reserve', 'reserve', 'tower', 'tower', 'tower',
				'school', 'school', 'school', 'wain', 'wain', 'fence',
				'fence', 'fort', 'fort', 'babylon', 'babylon', 'archer',
				'archer', 'archer', 'knight', 'knight', 'knight',
				'rider', 'rider', 'rider', 'platoon', 'platoon',
				'platoon', 'recruit', 'recruit', 'recruit', 'attack',
				'attack', 'saboteur', 'saboteur', 'thief', 'thief',
				'swat', 'swat', 'banshee', 'banshee', 'conjure_bricks',
				'conjure_bricks', 'conjure_bricks', 'conjure_crystals',
				'conjure_crystals', 'conjure_crystals',
				'conjure_weapons', 'conjure_weapons', 'conjure_weapons',
				'crush_briks', 'crush_briks', 'crush_briks',
				'crush_crystals', 'crush_crystals', 'crush_crystals',
				'crush_weapons', 'crush_weapons', 'crush_weapons',
				'sorcerer', 'sorcerer', 'sorcerer', 'dragon', 'dragon',
				'pixies', 'pixies', 'curse', 'curse']
		else:
			self.__pileDown = cards
		
	def top(self):
		self.__pileUp.append(self.__pileDown.pop())
		return self.__pileUp[-1]
		
	def shuffle(self):
		random.shuffle(self.__pileDown)
	
	def pickCard(self):
		return self.__pileDown.pop()
