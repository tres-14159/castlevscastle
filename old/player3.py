# -*- coding: utf-8 -*-

class Player:
	__castle = 30
	__wall = 10
	
	__builders = 2
	__bricks = 5
	
	__soldiers = 2
	__weapons = 5
	
	__wizards = 2
	__crystals = 5
	
	__hand = []
	
	def __init__(self, lifeCastle = 10, lifeWall = 10, builders = 2, bricks = 5, soldiers = 2, weapons = 5, wizards = 2, ):
		self.__castle = lifeCastle
		self.__wall = lifeWall
		
	def getAttack(self, damage):
		if self.__wall == 0:
			self.__castle = self.__castle - damage
			if self.__castle < 0:
				self.__castle = 0
		else:
			self.__wall = self.__wall - damage
			if (self.__wall < 0) :
				self.__castle = self.__castle - self.__wall
				self.__wall = 0
		
	def getCastleLife(self):
		return self.__castle
		
	def getWallLife(self):
		return self.__wall
		
	def getBuilders(self):
		return self.__builders
		
	def getBricks(self):
		return self.__bricks
		
	def getSoldiers(self):
		return self.__soldiers
		
	def getWeapons(self):
		return self.__weapons
		
	def getWizards(self):
		return self.__wizards
		
	def getCrystals(self):
		return self.__crystals
		
	def removeCastleLife(self, count):
		overCount = self.__castle - count
		if overCount < 0:
			self.__castle = 0
			return overCount
		else:
			self.__castle = overCount
			return 0
		
	def removeWallLife(self, count):
		overCount = self.__wall - count
		if overCount < 0:
			self.__wall = 0
			return overCount
		else:
			self.__wall = overCount
			return 0
		
	def removeBricks(self, count):
		overCount = self.__bricks - count
		if overCount < 0:
			self.__bricks = 0
			return overCount
		else:
			self.__bricks = overCount
			return 0
			
	def removeWeapons(self, count):
		overCount = self.__weapons - count
		if overCount < 0:
			self.__weapons = 0
			return overCount
		else:
			self.__weapons = overCount
			return 0
			
	def removeCrystals(self, count):
		overCount = self.__crystals - count
		if overCount < 0:
			self.__crystals = 0
			return overCount
		else:
			self.__crystals = overCount
			return 0
			
	def decreaseCastleLife(self, life, overZero = False):
		self.__castle = self.__castle - life
		if not overZero:
			if self.__castle < 0:
				self.__castle = 0
		
	def decreaseWallLife(self, life, overZero = False):
		self.__wall = self.__wall - life
		if not overZero:
			if self.__wall < 0:
				self.__wall = 0
		
	def decreaseBuilders(self, count, overZero = False):
		self.__builders = self.__builders - count
		if not overZero:
			if self.__builders < 0:
				self.__builders = 0
		
	def decreaseBricks(self, count, overZero = False):
		self.__bricks = self.__bricks - count
		if not overZero:
			if self.__bricks < 0:
				self.__bricks = 0
		
	def decreaseSoldiers(self, count, overZero = False):
		self.__soldiers = self.__soldiers - count
		if not overZero:
			if self.__soldiers < 0:
				self.__soldiers = 0
		
	def decreaseWeapons(self, count, overZero = False):
		self.__weapons = self.__weapons - count
		if not overZero:
			if self.__weapons < 0:
				self.__weapons = 0
		
	def decreaseWizards(self, count, overZero = False):
		self.__wizards = self.__wizards - count
		if not overZero:
			if self.__wizards < 0:
				self.__wizards = 0
		
	def decreaseCrystals(self, count, overZero = False):
		self.__crystals = self.__crystals - count
		if not overZero:
			if self.__crystals < 0:
				self.__crystals = 0
			
	def increaseCastleLife(self, life):
		self.__castle = self.__castle + life
		
	def increaseWallLife(self, life):
		self.__wall = self.__wall + life
		
	def increaseBuilders(self, count):
		self.__builders = self.__builders + count
		
	def increaseBricks(self, count):
		self.__bricks = self.__bricks + count
		
	def increaseSoldiers(self, count):
		self.__soldiers = self.__soldiers + count
		
	def increaseWeapons(self, count):
		self.__weapons = self.__weapons + count
		
	def increaseWizards(self, count):
		self.__wizards = self.__wizards + count
		
	def increaseCrystals(self, count):
		self.__crystals = self.__crystals + count
		
	def setCastleLife(self, life):
		self.__castle = life
		
	def setWallLife(self, life):
		self.__wall = life
		
	def setBuilders(self, count):
		self.__builders = count
		
	def setBricks(self, count):
		self.__bricks = count
		
	def setSoldiers(self, count):
		self.__soldiers = count
		
	def setWeapons(self, count):
		self.__weapons = count
		
	def setWizards(self, count):
		self.__wizards = count
		
	def setCrystals(self, count):
		self.__crystals = count
	
	def putHand(self, card):
		self.__hand.append(card)
	
	def getCardHand(self, pos):
		if (pos < len(self.__hand)):
			card = self.__hand[pos]
			del(self.__hand[pos])
			return card
		else:
			return None
		
	def showHand(self):
		return self.__hand
