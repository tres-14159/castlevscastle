# -*- coding: utf-8 -*- 

"""Module of multiples utils"""

class Singleton(type):
	"""Design pattern in a  metaclass."""
	def __init__(cls, name, bases, namespace):
		super(Singleton, cls).__init__(name, bases, namespace)
		cls._instance = None

	def __call__(cls, *args, **kwargs):
		if cls._instance is None:
			self = super(Singleton, cls).__call__(*args, **kwargs)
			cls._instance =  self
		return cls._instance
		
"""
#Example

class A:
	__metaclass__ = Singleton
	i = 0

	def __init__(self, z = 666):
		print ('a')
		self.i = z
		
a = A(3)
print a.i
b = A(4) #This not call __init__ because it's the first time.
print b.i
print a.i
"""
