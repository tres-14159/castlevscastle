# -*- coding: utf-8 -*-

import sys

from utils import *
from player import Player
from pile import Pile
from card import Card
from term import Term
from pygame import PyGame

class Game:
	__metaclass__ = Singleton
	
	__mode = None
	__modeObject = None
	
	__player1 = None
	__player2 = None
	
	__pile = None
	
	__turn = 0
	__activePlayer = None
	__otherPlayer = None
	
	def __init__(self, mode = 'term'):
		self.__1player = Player()
		self.__2player = Player()
		self.__pile = Pile()
		
		self.__mode = mode
	
	def iniGame(self):
		self.__pile.clean_pile()
		self.__pile.clean_discard()
		self.__pile.fill_pile()
		self.__pile.shuffle()
		
		for iterator in range(0, 5):
			card = self.__pile.get_card()
			self.__1player.addCardHand(card)
		
		for iterator in range(0, 5):
			card = self.__pile.get_card()
			self.__2player.addCardHand(card)
	
	def run(self):
		if self.__mode == 'term':
			self.__modeObject = Term()
		elif self.__mode == 'pygame':
			self.__modeClass = PyGame()
		
		self.__modeObject.showMenu()
		self.__modeObject.end()
	
	def runTerm(self):
		self.iniGame()
		
		toExit = False
		while not toExit:
			if self.__activePlayer == 1:
				self.__activePlayer = 2
				self.__otherPlayer = 1
			else:
				self.__activePlayer = 1
				self.__otherPlayer = 2
				self.__turn += 1
				print("Turn: %d" % self.__turn)
			
			if self.__activePlayer == 1:
				thePlayer = self.__1player
				otherPlayer = self.__2player
			else:
				thePlayer = self.__2player
				otherPlayer = self.__1player
				
			if self.__turn != 1:
				card = self.__pile.get_card()
				thePlayer.addCardHand(card)
			
			if self.__turn > 1 or self.__activePlayer != 1:
				self.increaseResources(thePlayer)
			
			endTurn = False
			while not endTurn:
				print("Player %d:" % self.__activePlayer)
				self.printStats(self.__activePlayer)
				print("Player %d:" % self.__otherPlayer)
				self.printStats(self.__otherPlayer)
				
				action = raw_input('Action S(T)atus other player, (S)how cards, (U)se card, (D)iscard, (E)xit: ')
				
				if action.upper() == 'T':
					self.printStats(self.__otherPlayer)
				elif action.upper() == 'S':
					self.printCards(self.__activePlayer)
				elif action.upper() == 'U':
					self.printCards(self.__activePlayer)
					card = raw_input('Please what number card: ')
					if card.isdigit():
						card = int(card)
						if card >= 0 and card <= 5:
							playerCard = thePlayer.getCardHand(card)
							objCard = Card(playerCard)
							if objCard.canPlayCard(thePlayer):
								objCard.playCard(thePlayer, otherPlayer)
								thePlayer.removeCardHand(card)
								self.__pile.discart_card(playerCard)
								endTurn = True
							else:
								print('No enough resources.')
						else:
							print('Wrong number')
				elif action.upper() == 'D':
					self.printCards(self.__activePlayer)
					card = raw_input('Please what number card: ')
					if card.isdigit():
						card = int(card)
						if card >= 0 and card <= 5:
							playerCard = thePlayer.removeCardHand(card)
							self.__pile.discart_card(card)
							endTurn = True
						else:
							print('Wrong number')
				elif action.upper() == 'E':
					endTurn = True
					toExit = True
				else:
					print('Wrong action')
				
				print("")
				
				if thePlayer.getCastle() >= 100 or otherPlayer.getCastle() == 0:
					print("Win %d player" % self.__activePlayer)
					toExit = True
	
	def increaseResources(self, player):
		player.increaseBricks(player.getBuilders())
		player.increaseWeapons(player.getSoldiers())
		player.increaseCrystals(player.getWizards())
	
	def printCards(self, player):
		if player == 1:
			showPlayer = self.__1player
		else:
			showPlayer = self.__2player
		
		hand = showPlayer.getHand()
		count = 0
		for card in hand:
			print(str(count) + ': ' + card + ' ')
			count += 1
	
	def printStats(self, player):
		if player == 1:
			showPlayer = self.__1player
		else:
			showPlayer = self.__2player
		
		print("Ca: %d Wa: %d Bu: %d Br: %d So: %d We: %d Wi: %d Cr: %d" % \
			(showPlayer.getCastle(), showPlayer.getWall(), showPlayer.getBuilders(), \
			showPlayer.getBricks(), showPlayer.getSoldiers(), showPlayer.getWeapons(), \
			showPlayer.getWizards(), showPlayer.getCrystals()))
			
	
	def runPygame(self):
		pass
