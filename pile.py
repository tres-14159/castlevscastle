# -*- coding: utf-8 -*-

import random

class Pile:
	__pile = []
	__discard = []
	
	def __init__(self):
		self.clean_pile()
		self.clean_discard()
	
	def clean_pile(self):
		self.__pile = []
	
	def clean_discard(self):
		self.__discard = []
	
	def get_card(self):
		return self.__pile.pop()
	
	def discart_card(self, card):
		self.__discard.append(card)
	
	def fill_pile(self):
		self.__pile.append('wall')
		self.__pile.append('wall')
		self.__pile.append('wall')
		
		self.__pile.append('base')
		self.__pile.append('base')
		self.__pile.append('base')
		
		self.__pile.append('defence')
		self.__pile.append('defence')
		self.__pile.append('defence')
		
		self.__pile.append('reserve')
		self.__pile.append('reserve')
		self.__pile.append('reserve')
		
		self.__pile.append('tower')
		self.__pile.append('tower')
		self.__pile.append('tower')
		
		self.__pile.append('school')
		self.__pile.append('school')
		self.__pile.append('school')
		
		self.__pile.append('wain')
		self.__pile.append('wain')
		
		self.__pile.append('fence')
		self.__pile.append('fence')
		
		self.__pile.append('fort')
		self.__pile.append('fort')
		
		self.__pile.append('babylon')
		self.__pile.append('babylon')
		
		self.__pile.append('archer')
		self.__pile.append('archer')
		self.__pile.append('archer')
		
		self.__pile.append('knight')
		self.__pile.append('knight')
		self.__pile.append('knight')
		
		self.__pile.append('rider')
		self.__pile.append('rider')
		self.__pile.append('rider')
		
		self.__pile.append('platoon')
		self.__pile.append('platoon')
		self.__pile.append('platoon')
		
		self.__pile.append('recruit')
		self.__pile.append('recruit')
		self.__pile.append('recruit')
		
		self.__pile.append('attack')
		self.__pile.append('attack')
		
		self.__pile.append('saboteur')
		self.__pile.append('saboteur')
		
		self.__pile.append('thief')
		self.__pile.append('thief')
		
		self.__pile.append('swat')
		self.__pile.append('swat')
		
		self.__pile.append('banshee')
		self.__pile.append('banshee')
		
		self.__pile.append('conjure_bricks')
		self.__pile.append('conjure_bricks')
		self.__pile.append('conjure_bricks')
		
		self.__pile.append('conjure_crystals')
		self.__pile.append('conjure_crystals')
		self.__pile.append('conjure_crystals')
		
		self.__pile.append('conjure_weapons')
		self.__pile.append('conjure_weapons')
		self.__pile.append('conjure_weapons')
		
		self.__pile.append('crush_bricks')
		self.__pile.append('crush_bricks')
		self.__pile.append('crush_bricks')
		
		self.__pile.append('crush_crystals')
		self.__pile.append('crush_crystals')
		self.__pile.append('crush_crystals')
		
		self.__pile.append('crush_weapons')
		self.__pile.append('crush_weapons')
		self.__pile.append('crush_weapons')
		
		self.__pile.append('sorcerer')
		self.__pile.append('sorcerer')
		self.__pile.append('sorcerer')
		
		self.__pile.append('dragon')
		self.__pile.append('dragon')
		
		self.__pile.append('pixies')
		self.__pile.append('pixies')
		
		self.__pile.append('curse')
		self.__pile.append('curse')
		
	def shuffle(self):
		random.shuffle(self.__pile, random.random)
