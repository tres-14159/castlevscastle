# -*- coding: utf-8 -*-
import sys

from game import Game

def main():
	gameMachine = Game('term')
	
	gameMachine.run()
	
	sys.exit(0)

if __name__ == '__main__':
    main()
