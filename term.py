# -*- coding: utf-8 -*-

import curses


class Term:
	stdsrc = None
	
	def __init__(self):
		# initscr() returns a window object representing the entire screen.
		self.stdscr = curses.initscr()
		# turn off automatic echoing of keys to the screen
		curses.noecho()
		# Enable non-blocking mode. Keys are read directly, without hitting enter.
		curses.cbreak()
		# Disable the mouse cursor.
		curses.curs_set(0)
		self.stdscr.keypad(1)
		curses.mousemask(1)
		# Enable colorous output.
		curses.start_color()
		curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
		curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_BLACK)
		self.stdscr.bkgd(curses.color_pair(2))
	
	def end(self):
		curses.nocbreak()
		self.stdscr.keypad(0)
		curses.mousemask(0)
		curses.echo()
		curses.endwin()
	
	def create_window(self, rows, cols, y, x, title):
		win = curses.newwin(rows, cols, y, x)
		
		win.box()
		win.move(0, 1)
		win.addstr(title)
		win.move(1, 1)
		return win
	
	def get_name(self, win):
		curses.echo()
		result = win.getstr()
		curses.noecho()
		
		return result
	
	def create_centered_window(self, rows, cols, title):
		y = (curses.LINES - rows) // 2
		x = (curses.COLS - cols) // 2
		return self.create_window(rows, cols, y, x, title)
	
	def showMenu(self):
		win = self.create_centered_window(10, 40, 'Introduzca su nombre')
		#name = self.get_name(win)
		while True:
			event = win.getch() 
			if event == ord("q"): break 
			if event == curses.KEY_MOUSE:
				_, mx, my, _, _ = curses.getmouse()
			y, x = win.getyx()
			win.addstr(y, x, win.instr(my, mx, 5))
		win.erase()