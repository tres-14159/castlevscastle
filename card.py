# -*- coding: utf-8 -*-

class Card:
	__card = None
	__cards = {
		'wall': {'br': 1, 'we': 0, 'cr': 0}, # fence +3
		'base': {'br': 1, 'we': 0, 'cr': 0}, # castle +2
		'defence': {'br': 3, 'we': 0, 'cr': 0}, # fence +6
		'reserve': {'br': 3, 'we': 0, 'cr': 0}, # castle +8, fence -4 (if it is 3 or less fence, it runs also)
		'fence': {'br': 3, 'we': 0, 'cr': 0}, # fence +6
		'tower': {'br': 5, 'we': 0, 'cr': 0}, # castle +5
		'school': {'br': 8, 'we': 0, 'cr': 0}, # builders +1
		'wain': {'br': 10, 'we': 0, 'cr': 0}, # castle 8, enemy castle - 4
		'fort': {'br': 18, 'we': 0, 'cr': 0}, # castle +20
		'babylon': {'br': 39, 'we': 0, 'cr': 0}, # castle +32
		
		'archer': {'br': 0, 'we': 1, 'cr': 0}, # attack 2
		'knight': {'br': 0, 'we': 2, 'cr': 0}, # attack 3
		'rider': {'br': 0, 'we': 2, 'cr': 0}, # attack 4
		'platoon': {'br': 0, 'we': 4, 'cr': 0}, # attack 6
		'recruit': {'br': 0, 'we': 8, 'cr': 0}, # soldiers +1
		'attack': {'br': 0, 'we': 10, 'cr': 0}, # attack 12
		'saboteur': {'br': 0, 'we': 12, 'cr': 0}, # enemy stocks -4
		'thief': {'br': 0, 'we': 15, 'cr': 0}, # transfer enemy stocks 5
		'swat': {'br': 0, 'we': 18, 'cr': 0}, # enemy castle -10
		
		'conjure_bricks': {'br': 0, 'we': 0, 'cr': 4}, # bricks +8
		'conjure_crystals': {'br': 0, 'we': 0, 'cr': 4}, # enemy crystals -8
		'conjure_weapons': {'br': 0, 'we': 0, 'cr': 4}, # weapons +8
		'crush_bricks': {'br': 0, 'we': 0, 'cr': 4}, # enemy bricks -8
		'crush_crystals': {'br': 0, 'we': 0, 'cr': 4}, # enemy crystals -8
		'crush_weapons': {'br': 0, 'we': 0, 'cr': 4}, # enemy weapons -8
		'sorcerer': {'br': 0, 'we': 0, 'cr': 8}, # magic +1
		'dragon': {'br': 0, 'we': 0, 'cr': 21}, # attack 25
		'pixies': {'br': 0, 'we': 0, 'cr': 22}, # castle +22
		'curse': {'br': 0, 'we': 0, 'cr': 45}} # all + 1, enemies all -1
	
	def __init__(self, card):
		self.__card = card
	
	def canPlayCard(self, player):
		if self.__cards[self.__card]['br'] > player.getBricks():
			return False
		
		if self.__cards[self.__card]['we'] > player.getWeapons():
			return False
		
		if self.__cards[self.__card]['cr'] > player.getCrystals():
			return False
		
		if self.__card == 'reserve':
			if player.getWall() < 4:
				return False
		
		return True
	
	def playCard(self, player, oponent):
		player.decreaseBricks(self.__cards[self.__card]['br'])
		player.decreaseWeapons(self.__cards[self.__card]['we'])
		player.decreaseCrystals(self.__cards[self.__card]['cr'])
		
		if self.__card == 'wall':
			player.increaseWall(3)
		elif self.__card == 'base':
			player.increaseCastle(2)
		elif self.__card == 'defence':
			player.increaseWall(6)
		elif self.__card == 'reserve':
			player.decreaseWall(-4)
			player.increaseCastle(8)
		elif self.__card == 'tower':
			player.increaseCastle(5)
		elif self.__card == 'school':
			player.increaseBuilders(1)
		elif self.__card == 'wain':
			#if oponent haven't less than 4 castle, the card stolen to 0
			#and the player sum 8
			oponent.decreaseCastle(4)
			player.increaseCastle(8)
		elif self.__card == 'fence':
			player.increaseWall(12)
		elif self.__card == 'fort':
			player.increaseCastle(20)
		elif self.__card == 'babylon':
			player.increaseCastle(32)
		elif self.__card == 'archer':
			oponent.attack(2)
		elif self.__card == 'knight':
			oponent.attack(3)
		elif self.__card == 'rider':
			oponent.attack(4)
		elif self.__card == 'platoon':
			oponent.attack(6)
		elif self.__card == 'recruit':
			player.increaseSoldiers(1)
		elif self.__card == 'attack':
			oponent.attack(12)
		elif self.__card == 'saboteur':
			oponent.decreaseBricks(4)
			oponent.decreaseWeapons(4)
			oponent.decreaseCrystals(4)
		elif self.__card == 'thief':
			#not 5 instead the resource have oponent bellow 5
			
			#The return is negative
			countBricks = 5 + oponent.decreaseBricks(5)
			countWeapons = 5 + oponent.decreaseWeapons(5)
			countCrystals = 5 + oponent.decreaseCrystals(5)
			
			player.increaseBricks(countBricks)
			player.increaseWeapons(countWeapons)
			player.increaseCrystals(countCrystals)
		elif self.__card == 'swat':
			oponent.decreaseCastle(10)
		elif self.__card == 'banshee':
			oponent.attack(32)
		elif self.__card == 'conjure_bricks':
			player.increaseBricks(8)
		elif self.__card == 'conjure_crystals':
			player.increaseCrystals(8)
		elif self.__card == 'conjure_weapons':
			player.increaseWeapons(8)
		elif self.__card == 'crush_bricks':
			#not 8 instead crush the number of resources that he have
			oponent.decreaseBricks(8)
		elif self.__card == 'crush_crystals':
			oponent.decreaseCrystals(8)
		elif self.__card == 'crush_weapons':
			oponent.decreaseWeapons(8)
		elif self.__card == 'sorcerer':
			player.increaseWizards(1)
		elif self.__card == 'dragon':
			oponent.attack(25)
		elif self.__card == 'pixies':
			player.increaseCastle(22)
		elif self.__card == 'curse':
			#Rest all althought 0 in any resource, and sum 1 in any
			#resource althought the oponent have 0
			oponent.decreaseBricks(1)
			oponent.decreaseBuilders(1)
			oponent.decreaseWeapons(1)
			oponent.decreaseSoldiers(1)
			oponent.decreaseCrystals(1)
			oponent.decreaseWizards(1)
			oponent.decreaseWall(1)
			oponent.decreaseCastle(1)
			
			player.increaseBricks(1)
			player.increaseBuilders(1)
			player.increaseWeapons(1)
			player.increaseSoldiers(1)
			player.increaseCrystals(1)
			player.increaseWizards(1)
			player.increaseWall(1)
			player.increaseCastle(1)
